package com.puntodamar.springrest.formatter;

import com.puntodamar.springrest.exception.ModelNotFoundException;
import com.puntodamar.springrest.exception.ResponseException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class ControllerResponseFormatter extends ResponseEntityExceptionHandler {

    private Throwable getRootCause(Throwable e) {
        Throwable cause = null;
        Throwable result = e;

        while(null != (cause = result.getCause())  && (result != cause) ) {
            result = cause;
        }
        return result;
    }

    @ExceptionHandler({Exception.class})
    public final ResponseEntity<Object> handleAllException(Exception e, WebRequest request){
        return new ResponseEntity<>(new ResponseException(e.getMessage(), getRootCause(e)), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ModelNotFoundException.class)
    public final ResponseEntity<Object> handleAllException(ModelNotFoundException e, WebRequest request){
        return new ResponseEntity<>(new ResponseException(e.getMessage(), getRootCause(e).getCause().getMessage()), HttpStatus.NOT_FOUND);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException e, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ResponseErrorFormatter responseErrorFormatter = new ResponseErrorFormatter("Validation error(s)");
        for(FieldError err : e.getBindingResult().getFieldErrors()){
            responseErrorFormatter.add(err.getField(), err.getDefaultMessage());
        }

        for (final ObjectError error : e.getBindingResult().getGlobalErrors()) {
            responseErrorFormatter.add(error.getObjectName(), error.getDefaultMessage());
        }
        return new ResponseEntity<>(responseErrorFormatter, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException e, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return new ResponseEntity<>(new ResponseException(e.getMessage(), e.getRootCause()), HttpStatus.BAD_REQUEST);
    }
    
}
