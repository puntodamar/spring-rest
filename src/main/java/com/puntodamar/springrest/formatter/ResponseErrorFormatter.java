package com.puntodamar.springrest.formatter;

import java.util.ArrayList;
import java.util.HashMap;

public class ResponseErrorFormatter {


    private final String message;
    private HashMap<String, ArrayList<String>> errors = new HashMap<>();

    public ResponseErrorFormatter(String message){
        this.message = message;
    }

    public ResponseErrorFormatter(String message, HashMap<String, ArrayList<String>> errorMessages){
        this.message = message;
        this.errors = errorMessages;
    }

    public String getMessage() {
        return message;
    }

    public HashMap<String, ArrayList<String>> getErrors() {
        return errors;
    }

    public void add(String field, String message){
        if(errors.containsKey(field)){
            ArrayList<String> currentMessages = errors.get(field);
            currentMessages.add(message);
            errors.put(field, currentMessages);
        }
        else {
            ArrayList<String> currentMessages = new ArrayList<>();
            currentMessages.add(message);
            errors.put(field, currentMessages);
        }
    }
//
//    public Map<String, Map<String, Serializable>> errorsMessages() {
//        return Map.of(
//                "error", Map.of(
//                        "message", "Validation Error(s)",
//                        "errors", this.errorMessages
//                )
//        );
//    }
}
