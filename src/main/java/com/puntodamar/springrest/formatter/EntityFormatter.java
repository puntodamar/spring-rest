package com.puntodamar.springrest.formatter;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.util.Map;

@Component
public class EntityFormatter {

    public static ResponseEntity<Object> format(Integer code, String fieldName, Object entity, URI location) {
        return ResponseEntity.created(location).body(Map.of(
                fieldName, entity,
                "code", code
        ));
    }

    public static ResponseEntity<Object> format(Integer code, String fieldName, Object entity) {
        return new ResponseEntity<>(Map.of(
                fieldName, entity,
                "code", code
        ), HttpStatus.valueOf(code));
    }

    public static ResponseEntity<Object> format(Integer code, Object entity) {
        return new ResponseEntity<>(Map.of(
                "data", entity,
                "code", code
        ), HttpStatus.valueOf(code));
    }

    public static ResponseEntity<Object> format(Object entity) {
        return new ResponseEntity<>(Map.of(
                "data", entity,
                "code", HttpStatus.OK.value()
        ), HttpStatus.OK);
    }


}
