package com.puntodamar.springrest.user;

import com.puntodamar.springrest.exception.ModelNotFoundException;
import com.puntodamar.springrest.formatter.EntityFormatter;
import com.puntodamar.springrest.post.Post;
import com.puntodamar.springrest.post.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.*;

@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PostRepository postRepository;

    @Autowired
    MessageSource messageSource;

//    public ResponseEntity<Object> all(@RequestHeader(name = "Accept-Language", required = false) Locale locale ) {
    @GetMapping("/users")
    public ResponseEntity<Object> all() {
        List<User> users = userRepository.findAll();
        ArrayList<UserEntity> userEntities = new ArrayList<>();
        for (User user : users) {
            UserEntity userEntity = new UserEntity(user);
            userEntity.addSelfReference();
            userEntities.add(userEntity);
        }
        Map<String, Object> data = Map.of(
                "message", messageSource.getMessage("good.morning.message", null, LocaleContextHolder.getLocale()),
                "users", userEntities
        );
        return EntityFormatter.format(data);
    }
//
//    @GetMapping("/users/names")
//    public ResponseEntity<Object> names() {
//        List<User> users = userRepository.findAll();
//        ArrayList<UserEntity> userEntities = new ArrayList<>();
//        for (User user : users) {
//            UserEntity userEntity = new UserEntity(user);
//            userEntity.addSelfReference();
//            userEntities.add(userEntity);
//        }
//        Map<String, Object> data = Map.of(
//                "message", messageSource.getMessage("good.morning.message", null, LocaleContextHolder.getLocale()),
//                "users", userEntities
//        );
//
//        return EntityFormatter.format(data);
//    }


    @GetMapping("/users/{id}")
    public ResponseEntity<Object> detail(@PathVariable long id) {
        Optional<User> user = userRepository.findById(id);
        if(user.isEmpty()) throw new ModelNotFoundException(User.class, "User with id " + id + " not found");
        UserEntity entity = new UserEntity(user);
        return EntityFormatter.format(entity);
    }

    @PostMapping("/users")
    public ResponseEntity<Object> create(@Valid @RequestBody User user){
        User savedUser = userRepository.save(user);
        UserEntity userEntity = new UserEntity(savedUser);
        userEntity.addSelfReference();
        return EntityFormatter.format(HttpStatus.CREATED.value(), "user", userEntity);
    }

    @DeleteMapping("/users/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable long id) {
        userRepository.deleteById(id);
    }

    @PostMapping("/users/{id}/posts")
    public ResponseEntity<Object> addPost(@PathVariable long id, @RequestBody Post post) {
        Optional<User> user = userRepository.findById(id);
        if(user.isEmpty()) throw new ModelNotFoundException(User.class, "User with id " + id + " not found");
        post.setAuthor(user.get());
        postRepository.save(post);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(post.getId()).toUri();
        return EntityFormatter.format(HttpStatus.CREATED.value(), "post", post, location);

    }
}
