package com.puntodamar.springrest.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

@Component
public class UserCmdRunner implements CommandLineRunner {

    @Autowired
    private UserRepository userRepository;

    private static final Logger logger = LoggerFactory.getLogger(UserCmdRunner.class);
    @Override
    public void run(String... args) throws Exception {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1988);
        cal.set(Calendar.MONTH, Calendar.JANUARY);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        User user = new User("Jack", cal.getTime(), "jack@email.com", "passworddadadadadad");
        userRepository.save(user);
        logger.info("New user is created" + user);
        Optional<User> findById = userRepository.findById(1L);
        logger.info("Find user " + findById);
    }
}
