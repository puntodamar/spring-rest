package com.puntodamar.springrest.user;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.puntodamar.springrest.post.Post;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@ApiModel(description = "All details about the user")
@Entity
public class User {
    @Id
    @GeneratedValue
    private long id;

    @Size(min = 2, message = "minumum length is 2")
    private String name;

    @Length(min=1)
    @Email(message = "is invalid")
    private String email;

    @Length(min=7)
    @JsonIgnore
    private String password;

    @Past
    @ApiModelProperty(notes = "Cannot be future dates")
    private Date birthday;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "author")
    private List<Post> posts;


    public User(String name, Date birthday, String email, String password) {
        this.email = email;
        this.name = name;
        this.birthday = birthday;
        this.password = password;
    }

    public User() { }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public List<Post> getPosts() {
        return posts;
    }

}
