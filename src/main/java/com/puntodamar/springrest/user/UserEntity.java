package com.puntodamar.springrest.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.RepresentationModel;

import java.util.Optional;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

//@JsonFilter("UserOnlyName")
public class UserEntity extends RepresentationModel<UserEntity> {

    private final Optional<User> user;

    public UserEntity(@JsonProperty("user") Optional<User> user) {
        this.user = user;
    }

    public UserEntity(@JsonProperty("user") User user) {
//        Optional<User> optional = user;
        this.user = Optional.of(user);
    }

    public void addSelfReference() {
        this.add(linkTo(methodOn(UserController.class).detail((int) user.get().getId())).withSelfRel());
    }

    public Optional<User> getUser() { return user; }

}
